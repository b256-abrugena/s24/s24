// console.log("Testing");

// First Task
let inputNumber2 = Number(prompt('Enter a number: '));
let getCube = inputNumber2 ** 3;

console.log("The cube of " + inputNumber2 + " is " + getCube);

// Second Task
let address = ['258 Washington Ave', 'NW', 'California', 90011];
console.log(`I live in ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`);

let animal = {
	name: 'Lolong',
	animalType: 'saltwater crocodile',
	weightKg: 1075, 
	lengthFeet: 20,
	lengthIn: 3,
}
console.log(`${animal.name} was a ${animal.animalType}. He weighed at ${animal.weightKg} kgs with a measurement of ${animal.lengthFeet} ft ${animal.lengthIn} in.`);

// Third Task
const arrayNum = [1, 2, 3, 4, 5];

arrayNum.forEach(num => {
	console.log(num);
})

// Fourth Task
let reduceNumber = arrayNum.reduce((x,y) => {
	return x + y;
})

console.log(reduceNumber);

// Fifth Task
function Dog(name, age, breed){
	this.name = name;
	this.age = age;
	this.breed = breed;
}

let dog1 = new Dog("Frankie", 5, "Miniature Dachshund");
let dog2 = new Dog("Doge", 13, "Shiba Inu");
let dog3 = new Dog("Mishka", 7, "Siberian Husky");

console.log(dog1);
console.log(dog2);
console.log(dog3);

